import { useState } from "react";

function useInput(defaultValue = "") {
  const [value, setInput] = useState(defaultValue);
  function onChange(e) {
    setInput(e.target.value);
  }
  function reset() {
    setInput("");
  }
  return [value, onChange, reset];
}

export default useInput;
