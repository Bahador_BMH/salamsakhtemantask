import { useState } from "react";
import SimpleReactValidator from "simple-react-validator";
//////////////////////////////////////////////////////////////////////////////////// css styles ///////////////////////////////////////////////////////////////////////////////////

import styles from "../../styles/Form.module.css";

//////////////////////////////////////////////////////////////////////////////////// material ui components ///////////////////////////////////////////////////////////////////////////////////

import { Grid, TextField, Snackbar, Fade, IconButton } from "@mui/material";
import Cancel from "@mui/icons-material/Cancel";
import Check from "@mui/icons-material/CheckOutlined";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";

//////////////////////////////////////////////////////////////////////////////////// action component ///////////////////////////////////////////////////////////////////////////////////

import { sendData } from "../actions/SendData";

//////////////////////////////////////////////////////////////////////////////////// custom components ///////////////////////////////////////////////////////////////////////////////////

import useInput from "../components/useInput";

//////////////////////////////////////////////////////////////////////////////////// function begining ///////////////////////////////////////////////////////////////////////////////////

const Form = (props) => {
  //////////////////////////////////////////////////////////////////////////////////// react hooks ///////////////////////////////////////////////////////////////////////////////////

  const [city, setCity, cityRst] = useInput("");
  const [region, setRegion, regionRst] = useInput("");
  const [name, setName, nameRst] = useInput("");
  const [phone, setPhone, phoneRst] = useInput("");
  const [tableData, setTableData] = useState([]);
  const [open, setOpen] = useState(false);
  const [openC, setOpenC] = useState(false);
  const [error, setError] = useState("");

  //////////////////////////////////////////////////////////////////////////////////// global variables ///////////////////////////////////////////////////////////////////////////////////
  let validator = new SimpleReactValidator();
  //////////////////////////////////////////////////////////////////////////////////// functions ///////////////////////////////////////////////////////////////////////////////////

  const submitData = () => {
    if (city !== "" && region !== "" && name !== "" && phone !== "") {
      setTableData([city, region, name, phone]);
    } else {
      setOpen(true);
      setError("از خالی نبودن فیلد ها اطمینان حاصل نمایید");
    }
  };

  const finalSubmit = () => {
    if (tableData.length == 4) {
      setOpenC(true);
      sendData(props, tableData); // sending the data to action function for post requrest
      cityRst("");
      regionRst("");
      nameRst("");
      phoneRst("");
      setTableData([]);
    } else {
      setOpen(true);
      setError("از ثبت اطلاعات اطمینان حاصل فرمایید");
    }
  };
  const handleClose = () => {
    setOpen(false);
    setOpenC(false);
  };
  //////////////////////////////////////////////////////////////////////////////////// jsx begining ///////////////////////////////////////////////////////////////////////////////////
  return (
    <Grid item xs={12}>
      <Grid container justifyContent="center">
        <Grid
          item
          xs={11}
          sm={11}
          md={10}
          lg={10}
          xl={8}
          className={styles.item}
          component={Paper}
        >
          <Grid container justifyContent="center">
            <Grid item xs={12}>
              <p className={styles.title}>فرم تکمیل اطلاعات</p>
            </Grid>
            {/* //////////////////////////////////////////////////////////////////////////////////// TextFields ///////////////////////////////////////////////////////////////////////////////////} */}
            <Grid
              item
              xs={10}
              sm={10}
              md={6}
              lg={6}
              xl={6}
              className={styles.fitem}
            >
              <Grid container justifyContent="center">
                <Grid
                  item
                  xs={12}
                  sm={10}
                  md={6}
                  lg={6}
                  xl={6}
                  className={styles.fieldsItems}
                >
                  <TextField
                    name="city"
                    title="city"
                    variant="standard"
                    label="شهر"
                    className={styles.firstFields}
                    value={city}
                    onChange={setCity}
                    required
                    error={city !== "" ? false : true} // validator and error handler
                  />
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={10}
                  md={6}
                  lg={6}
                  xl={6}
                  className={styles.fieldsItems}
                >
                  <TextField
                    name="region"
                    title="region"
                    variant="standard"
                    label="استان"
                    className={styles.firstFields}
                    value={region}
                    onChange={setRegion}
                    required
                    error={region !== "" ? false : true} // validator and error handler
                  />
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={10}
                  md={6}
                  lg={6}
                  xl={6}
                  className={styles.secondFieldsItems}
                >
                  <TextField
                    name="name"
                    title="name"
                    variant="standard"
                    label="نام و نام خانوادگی"
                    className={styles.firstFields}
                    value={name}
                    onChange={setName}
                    required
                    error={name === ""} // validator and error handler
                  />
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={10}
                  md={6}
                  lg={6}
                  xl={6}
                  className={styles.secondFieldsItems}
                >
                  <TextField
                    name="phone"
                    title="phone"
                    variant="standard"
                    label="شماره موبایل"
                    className={styles.firstFields}
                    value={phone}
                    onChange={setPhone}
                    required
                    error={
                      validator.check(phone, "required|numeric") == true || // validator and error handler
                      phone !== ""
                        ? false
                        : true
                    }
                  />
                </Grid>
              </Grid>
            </Grid>
            {/* //////////////////////////////////////////////////////////////////////////////////// submit ///////////////////////////////////////////////////////////////////////////////////} */}
            <Grid item xs={12}>
              <Grid container justifyContent="center">
                <Grid
                  item
                  className={styles.submit}
                  onClick={() => submitData()}
                >
                  <p className={styles.subTag}>ثبت</p>
                </Grid>
              </Grid>
            </Grid>
            {/* //////////////////////////////////////////////////////////////////////////////////// table ///////////////////////////////////////////////////////////////////////////////////} */}
            <Grid item xs={12} className={styles.table}>
              <Grid container justifyContent="center">
                <Grid item xs={11} sm={10} md={10} lg={10} xl={8}>
                  <TableContainer component={Paper}>
                    <Table
                      sx={{ minWidth: 700 }}
                      aria-label="customized table"
                      style={{ direction: "rtl" }}
                    >
                      <TableHead>
                        <TableRow>
                          <TableCell align="center">شهر</TableCell>
                          <TableCell align="center">استان</TableCell>
                          <TableCell align="center">
                            نام و نام خانوادگی
                          </TableCell>
                          <TableCell align="center">شماره موبایل</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {tableData.map((row, i) => (
                          <TableCell key={i} align="center" scope="row">
                            <p>{row}</p>
                          </TableCell>
                        ))}
                      </TableBody>
                    </Table>
                  </TableContainer>
                </Grid>
              </Grid>
            </Grid>
            {/* //////////////////////////////////////////////////////////////////////////////////// send data ///////////////////////////////////////////////////////////////////////////////////} */}
            <Grid item xs={12}>
              <Grid container justifyContent="center">
                <Grid item className={styles.submit} onClick={finalSubmit}>
                  <p className={styles.subTag}>ارسال اطلاعات</p>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      {/* //////////////////////////////////////////////////////////////////////////////////// snackbars ///////////////////////////////////////////////////////////////////////////////////} */}
      <Snackbar
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        variant="success"
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
        ContentProps={{
          "aria-describedby": "message-id",
        }}
        message={<p className={styles.snackbarPC}>{error}</p>}
        action={[
          <IconButton
            onClick={handleClose}
            key="close"
            aria-label="Close"
            color="inherit"
          >
            <Cancel className={styles.cancel} />
          </IconButton>,
        ]}
      />
      <Snackbar
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        className={styles.snackbar}
        variant="success"
        open={openC}
        onClose={handleClose}
        TransitionComponent={Fade}
        ContentProps={{
          "aria-describedby": "message-id",
        }}
        message={<p className={styles.snackbarP}>اطلاعات ارسال شد</p>}
        action={[
          <IconButton
            onClick={handleClose}
            key="close"
            aria-label="Close"
            color="inherit"
          >
            <Check className={styles.check} />
          </IconButton>,
        ]}
      />
    </Grid>
  );
};

export default Form;
