import axios from "axios"; // axios for req
import SEND_DATA_API from "../api/SendDataApi"; // send data api

export const sendData = async (props, information) => {
  try {
    let url;
    url = SEND_DATA_API;
    const res = await axios({
      method: "POST",
      url: url,
      data: information, // user data
      headers: {
        "Content-Type": "application/json",
      },
    });
    let data = res.data;
    console.log(data); // log of the request
    return data;
  } catch (err) {
    console.log(err); // error log
  }
};
